# README #


## What is this repository for? ##

The XML-Schema-Doc application takes a pointer a documentation project, which is a set of Java properties.  Current projects are included in the application itself, but there is no reason why they can't be referenced outside of it instead.

An Apache XML Schema parser is used to read the schema and generate documentation.  XML elements and attributes found are checked against the properties file to see if any additional documentation should be included.  
This is a good way to "keep notes" about tag and attribute use that can be re-used from version to version.

A summary of all elements is generated, with links to element details that includes:

* Element name and type.
* Annotations and document notes.
* List of attributes with type and accepted values.
* Information on what may be contained in this element, such as a sequence or choice of sub-elements.

Version 1.0

## How do I get set up? ##

### Set Up  ###

* Clone this repository to your local environment.
* Import it as a Maven project into Eclipse.
* Create a doc project by copying camel.properties or one of the others in src/main/resources/projects
* Edit the doc project with your own information (see comments embedded)
* Right-click the xml-sechema-doc project as Run as a Java application.

Results will be written to the "doc.output" file name.

### Configuration ###

* It's all in the doc project properties file.

### Dependencies ###

* Relies on org.apache.ws.commons.schema-XmlSchema-2.2.1.  JAR is bundled into the application.

### Database Configuration ###

* No database is used in this application.

### How to Run Tests ###

* Currently, there are no unit tests.

### Deployment ###

* In theory, you could package this as a JAR and run it using Java, but it's just simpler to run it in Eclipse.


## Contribution guidelines ##

* Writing tests - this would be welcome.
* Code review - all suggestions for improvement are also welcome.
* Other guidelines
** http://ws.apache.org/xmlschema - Apache XML Schema Parser
** http://ws.apache.org/xmlschema/xmlschema-core/apidocs/index.html - JavaDocs for parser

## Who do I talk to? ##

This project was created Mark Norton (mailto:mnorton@ms3-inc.com) on Mar. 27, 2017

* Other community or team contact
