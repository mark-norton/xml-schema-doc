package com.ms3_inc.util;

import java.io.FileWriter;
import java.io.IOException;

/**
 * DocWriter is a convenience class that supports writing to a FileWriter stream.
 * 
 * @author Mark Norton
 *
 */
public class DocWriter {
	public static String format = DocWriter.FORMAT_HTML;

	public static final String FORMAT_HTML = "html";
	public static final String FORMAT_WIKI = "wiki";
	public static final String FORMAT_TEXT = "text";
	
	private FileWriter out = null;

	/**
	 * Constructor given a file name.
	 * If the given file name exists, it will be over written with new data.
	 * 
	 * @param filename
	 * @throws IOException if an error on open.
	 */
	public DocWriter (String filename) throws IOException {
		if (filename == null)
			throw new IOException("File name passed is null.");
		this.out = new FileWriter(filename);
	}
	
	/**
	 * Constructor given a FileWriter.
	 * @param out
	 */
	public DocWriter(FileWriter out) throws IOException {
		if (out == null)
			throw new IOException("FileWrite object passed is null.");
		this.out = out;
	}


	/*************************************************************
	 * Setters and Getters
	 *************************************************************/
	
	/**
	 * @return the out
	 */
	public FileWriter getOut() {
		return out;
	}

	/**
	 * @param out the out to set
	 */
	public void setOut(FileWriter out) {
		this.out = out;
	}

	/**
	 * @return the format
	 */
	public String getFormat() {
		return DocWriter.format;
	}

	/**
	 * @param format the format to set
	 */
	public void setFormat(String format) {
		DocWriter.format = format;
	}

	/**
	 * Close the output file and finalize it.
	 */
	public void close() {
		try {
			this.out.close();
		}
		catch (IOException io) {
			io.printStackTrace();
		}
		finally {
			this.out = null;
		}
	}

	
	/*************************************************************
	 * Print Methods
	 *************************************************************/
	
	/**
	 * Write the string to the file.
	 * 
	 * @param str
	 * @throws IOException on write error.
	 */
	public void print(String str)  {
		char[] chars = str.toCharArray();
		try {
			this.out.write(chars, 0, str.length());
		}
		catch (IOException io) {
			//	A write exception is pretty unlikely.
			io.printStackTrace();
		}
	}
	
	/**
	 * Write the string with new line to the file.
	 * 
	 * @param str
	 * @throws IOException on write error.
	 */
	public void println(String str) {
		this.print(str+"\n");
	}
	
	/**
	 * Replace all single quotes with double quotes and write to the file.
	 * 
	 * @param str
	 * @throws IOException on write error.
	 */
	public void printQuote(String str) {
		String replaced = str.replaceAll("'", "\"");
		this.print(replaced);
	}

	/**
	 * Replace all single quotes with double quotes and write to the file with a new line.
	 * 
	 * @param str
	 * @throws IOException on write error.
	 */
	public void printlnQuote(String str) {
		String replaced = str.replaceAll("'", "\"");
		this.println(replaced);
	}
	
	/**
	 * Indents the string by the given amount and replaces single quotes with double.
	 * 
	 * @param indent
	 * @param str
	 */
	public void printIndent(int indent, String str) {
		for (int i=0; i<indent; i++)
			this.print("\t");
		this.printQuote(str);
	}

	/**
	 * Indents the string by the given amount and replaces single quotes with double.
	 * 
	 * @param indent
	 * @param str
	 */
	public void printlnIndent(int indent, String str) {
		for (int i=0; i<indent; i++)
			this.print("\t");
		this.printlnQuote(str);
	}
	
	/*************************************************************
	 * Formatted Output Methods
	 *************************************************************/

	/**
	 * Output a document header.
	 * 
	 * @param level - 1 to 5
	 * @param title - header content
	 */
	public void header(int indent, int level, String title) {
		for (int i=0; i<indent; i++)
			this.print("\t");

		//	Format as HTML
		if (DocWriter.format.compareTo(DocWriter.FORMAT_HTML) == 0) {
			this.println("<h"+level+">"+title+"</h"+level+">");
		}
		
		//	Format as Wiki
		else if (DocWriter.format.compareTo(DocWriter.FORMAT_WIKI) == 0) {
			this.println(".h"+level+" "+title);
		}
		
		//	Otherwise format as Text
		else {
			this.println(title);
		}
	}
	
	public void line(int indent, String content) {
		for (int i=0; i<indent; i++)
			this.print("\t");

		//	Format as HTML
		if (DocWriter.format.compareTo(DocWriter.FORMAT_HTML) == 0) {
			this.println(content+"<br/>");
		}
		
		//	Format as Wiki
		else if (DocWriter.format.compareTo(DocWriter.FORMAT_WIKI) == 0) {
			this.println(content);
		}
		
		//	Otherwise format as Text
		else {
			this.println(content);
		}
	}
	
	/**
	 * Format a page anchor point.
	 * 
	 * @param name
	 * @return anchor markup.
	 */
	public void anchor(String name) {
		if (DocWriter.format.compareTo(DocWriter.FORMAT_HTML) == 0)  {
			this.printQuote( "\n<a name='"+name+"' />\n");
		}
		else if (DocWriter.format.compareTo(DocWriter.FORMAT_WIKI) == 0)  {
			this.printQuote("\nanchor: "+name+"\n");
		}
		else   {
			this.printQuote("\nanchor: "+name+"\n");
		}		
	}

	/**
	 * Format an HTML link.
	 * 
	 * If title is passed as null, the url is used at the title.
	 * 
	 * @param url - link target.
	 * @param title - link title.
	 * @return anchor markup.
	 */
	public void link(String url, String title) {
		if (title == null)
			title = url;
		
		if (DocWriter.format.compareTo(DocWriter.FORMAT_HTML) == 0)  {
			this.printQuote( "<a href='"+url+"'>"+title+"</a>");
		}
		else if (DocWriter.format.compareTo(DocWriter.FORMAT_WIKI) == 0)  {
			this.printQuote("[["+url+"|"+title+"]]");
		}
		else   {
			this.printQuote(title+" ("+url+")");
		}
	}

}
