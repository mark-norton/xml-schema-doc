package com.ms3_inc.util;

import java.util.Comparator;

import javax.xml.namespace.QName;

import org.apache.ws.commons.schema.XmlSchemaElement;

/**
 * Implements a comparator that allows a list of XML schema elments to be sorted.
 * 
 * @author Mark J. Norton - mnorton@ms3-inc.com
 *
 */
public class CompareXmlSchemaElement implements Comparator<XmlSchemaElement> {

	/**
	 * @returns -1 if lesser, 0 if equal, 1 if greater
	 */
	public int compare(XmlSchemaElement element1, XmlSchemaElement element2) {
		QName qname1 = element1.getQName();
		QName qname2 = element2.getQName();
		
		String name1 = qname1.getLocalPart();
		String name2 = qname2.getLocalPart();
		
		return name1.compareTo(name2);
	}

}
