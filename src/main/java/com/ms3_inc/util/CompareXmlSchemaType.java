package com.ms3_inc.util;

import java.util.Comparator;

import javax.xml.namespace.QName;

import org.apache.ws.commons.schema.XmlSchemaType;

/**
 * Allows a list of XML Schema Types to be sorted.
 * 
 * @author Mark J. Norton - mnorton@ms3-inc.com
 *
 */
public class CompareXmlSchemaType implements Comparator<XmlSchemaType> {

	/**
	 * @returns -1 if lesser, 0 if equal, 1 if greater
	 */
	public int compare(XmlSchemaType type1, XmlSchemaType type2) {
		QName qname1 = type1.getQName();
		QName qname2 = type2.getQName();
		
		String name1 = qname1.getLocalPart();
		String name2 = qname2.getLocalPart();
		
		return name1.compareTo(name2);
	}

}
