package com.ms3_inc.util;

/**
 * A utility class that returns formatting strings that can be in-lined for markup.
 * These currently include:
 * <ul>
 * <li>Bold</li>
 * <li>Italic</li>
 * <li>Unordered List<li>
 * <li>Numbered List<li>
 * <li>Table with Headers</li>
 * </ul>
 * @author Mark J. Norton - mnorton@ms3-inc.com
 *
 */
public class Format {
	public static enum Order {START, END};
	public static enum List {UNORDERED, ORDERED};

	/**
	 * Return a bold markup string.
	 * 
	 * @param order - START or END
	 * @return a bold markup string
	 */
	public static String bold(String text) {
		if (DocWriter.format.compareTo(DocWriter.FORMAT_HTML) == 0)  {
			return "<b>"+text+"</b>";
		}
		if (DocWriter.format.compareTo(DocWriter.FORMAT_WIKI) == 0)  {
			return "**"+text+"**";
		}
		else
			return text;
	}

	/**
	 * Return an italic markup string.
	 * @param order
	 * @return
	 */
	public static String italic(String text) {
		if (DocWriter.format.compareTo(DocWriter.FORMAT_HTML) == 0)  {
			return "<i>"+text+"</i>";
		}
		if (DocWriter.format.compareTo(DocWriter.FORMAT_WIKI) == 0)  {
			return "//"+text+"//";
		}
		else
			return text;
	}

	/**
	 * Unordered list.
	 * @param order
	 * @return
	 */
	public static String list(Order order) {
		if (DocWriter.format.compareTo(DocWriter.FORMAT_HTML) == 0)  {
			if (order == Order.START)
				return "<ul>";
			else
				return "</ul>";
		}
		else if (DocWriter.format.compareTo(DocWriter.FORMAT_WIKI) == 0)  {
			return "";
		}
		else   {
			return "";
		}
	}
	
	public static String numList(Order order) {
		if (DocWriter.format.compareTo(DocWriter.FORMAT_HTML) == 0)  {
			if (order == Order.START)
				return "<ol>";
			else
				return "</ol>";
		}
		else if (DocWriter.format.compareTo(DocWriter.FORMAT_WIKI) == 0)  {
			return "";
		}
		else   {
			return "";
		}
	}
	
	/**
	 * List item.
	 * 
	 * @param text
	 * @return
	 */
	public static String item(List type, String text) {
		if (DocWriter.format.compareTo(DocWriter.FORMAT_HTML) == 0)  {
			return "<li>"+text+"</li>";
		}
		else if (DocWriter.format.compareTo(DocWriter.FORMAT_WIKI) == 0)  {
			if (type == List.ORDERED)
				return "# "+text;
			else
				return "* "+text;
		}
		else   {
			return text;
		}	
	}
	
	
	/**
	 * Start and end a table.
	 * 
	 * @param order
	 * @return
	 */
	public static String table(Order order) {
		if (DocWriter.format.compareTo(DocWriter.FORMAT_HTML) == 0)  {
			if (order == Order.START)
				return "<table border='1' cellpadding='4' cellspacing='0'>";
			else
				return "</table>";
		}
		else if (DocWriter.format.compareTo(DocWriter.FORMAT_WIKI) == 0)  {
			return "";
		}
		else   {
			return "";
		}		
	}
	
	/**
	 * Header row for a table.
	 * 
	 * @param cols
	 * @return
	 */
	public static String header(String[] cols) {
		StringBuffer header = new StringBuffer();
		
		if (DocWriter.format.compareTo(DocWriter.FORMAT_HTML) == 0)  {
			header.append("<tr>");
			for (String col : cols) {
				header.append("<th>"+col+"</th>");
			}
			header.append("</tr>");
			return header.toString();
		}
		else if (DocWriter.format.compareTo(DocWriter.FORMAT_WIKI) == 0)  {
			for (String col : cols) {
				header.append("|="+col);
			}
			header.append("|");
			return header.toString();
		}
		else   {
			for (String col : cols) {
				header.append(col+"  ");
			}
			return header.toString();
		}		
	}
	
	/**
	 * Table row.
	 * 
	 * @param cols
	 * @return
	 */
	public static String row(String[] cols) {
		StringBuffer row = new StringBuffer();
		
		if (DocWriter.format.compareTo(DocWriter.FORMAT_HTML) == 0)  {
			row.append("<tr>");
			for (String col : cols) {
				row.append("<td>"+col+"</td>");
			}
			row.append("</tr>");
			return row.toString();
		}
		else if (DocWriter.format.compareTo(DocWriter.FORMAT_WIKI) == 0)  {
			for (String col : cols) {
				row.append("|="+col);
			}
			row.append("|");
			return row.toString();
		}
		else   {
			for (String col : cols) {
				row.append(col+"  ");
			}
			return row.toString();
		}		
	}
	
	/**
	 * Format an HTML link.
	 * 
	 * If title is passed as null, the url is used at the title.
	 * 
	 * @param url - link target.
	 * @param title - link title.
	 * @return anchor markup.
	 */
	public static String link(String url, String title) {
		if (title == null)
			title = url;
		
		if (DocWriter.format.compareTo(DocWriter.FORMAT_HTML) == 0)  {
			return "<a href='"+url+"'>"+title+"</a>";
		}
		else if (DocWriter.format.compareTo(DocWriter.FORMAT_WIKI) == 0)  {
			return "[["+url+"|"+title+"]]";
		}
		else   {
			return title+" ("+url+")";
		}
	}
	
}
