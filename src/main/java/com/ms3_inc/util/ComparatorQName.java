package com.ms3_inc.util;

import java.util.Comparator;

import javax.xml.namespace.QName;

/**
 * Allows QNames to be sorted in lists.
 * 
 * @author Mark J. Norton - mnorton@ms3-inc.com
 *
 */
public class ComparatorQName implements Comparator<QName> {

	/**
	 * Compare two QNames 
	 * @param arg0
	 * @param arg1
	 * @return -1 if less, 0 if equal, 1 if greater.
	 */
	public int compare(QName arg0,  QName arg1) {
		String name0 = arg0.getLocalPart();
		String name1 = arg0.getLocalPart();
		return name0.compareTo(name1);
	}

}
