package com.ms3_inc.doc;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import java.util.*;

import javax.xml.transform.stream.StreamSource;

import javax.xml.namespace.QName;

import org.apache.ws.commons.schema.*;
import org.apache.ws.commons.schema.utils.XmlSchemaRef;
import org.w3c.dom.*;


import com.ms3_inc.util.CompareXmlSchemaElement;
import com.ms3_inc.util.CompareXmlSchemaType;
import com.ms3_inc.util.DocWriter;
import com.ms3_inc.util.Format;
import com.ms3_inc.util.Format.Order;

/**
 * <h2>XML Schema Documentation Generator</h2>
 * 
 * <p>
 * It is often the case that developers are tasked with writing XML documents that will be validated against an existing XML Schema (XSD).  Sadly, documentation other
 * than the schema itself is either missing or inadequate.  This application will generate formatted documentation from an XML Schema reference
 * </p>
 * 
 * @author Mark J. Norton - mnorton@ms3-inc.com
 *
 */
public class XmlSchemaDoc {
	public static XmlSchemaDoc app = null;

	//	These project values are initialized from the project properties file.
	private String projectName = null;
	private Properties properties = null;
	private XmlSchema schema = null;
	private DocWriter out = null;

	//	These lists are generated into documentation sections.
	private List<XmlSchemaElement> elements = null;
	private List<XmlSchemaType> types = null;

	
	/**
	 * App constructor given a project name.
	 * 
	 * @param projectName
	 */
	public XmlSchemaDoc(String projectName) {
		this.projectName = projectName;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		if (args.length != 1 || args[0] == null) {
			System.out.println("Generation Terminated:  no project name was specified.");
			return;
		}
		
		//	Instantiate the application object.
		String projectName = args[0];
		XmlSchemaDoc.app = new XmlSchemaDoc(projectName);
		
		app.initProject();
		app.generate();
		app.closeProject();
		
		System.out.println("Generation complete.");
	}
	
	/**
	 * Initialize the document generation project.  This accomplished by:
	 * <ol>
	 * <li>Open the project properties file</li>
	 * <li>Open and parse the XML schema file</li>
	 * <li>Open the output document file.</li>
	 * </ol>
	 * 
	 */
	public void initProject() {
		InputStream in = null;
		
		//	Project loading and initialization.
		try {
			System.out.println("XML Schema Documentation Generator");

			//	1.  Open notes properties file.
			File f = new File(this.projectName);
			InputStream pin = new FileInputStream(f);
			this.properties = new Properties();
			this.properties.load(pin);

			String docName = this.properties.getProperty("doc.name", "Unknown");
			System.out.println("Preparing to generate: "+docName);

			//	2.  Open and parse the XML Schema reference.
			String urlStr = this.properties.getProperty("doc.url");
			if (urlStr == null) {
				System.out.println("Generation terminated:  schema URL was not found in the project properties.");
				return;
			}
			
			//	Get the text of the XML schema.
			URL url = new URL(urlStr);
			in = url.openStream();
			
			//	Parse the XML schema file.
			XmlSchemaCollection schemaCol = new XmlSchemaCollection();
			this.schema = schemaCol.read(new StreamSource(in));
			System.out.println("XML Schema file parsed:  "+urlStr);
			
			//	Extract the list of elements and sort them.
			Map<QName,XmlSchemaElement> eleMap = this.schema.getElements();
			this.elements = new ArrayList<XmlSchemaElement>(eleMap.values());
			Comparator<XmlSchemaElement> compE = new CompareXmlSchemaElement();
			Collections.sort(this.elements, compE);
			
			//	Extract the lsit of types and sort them.
			Map<QName,XmlSchemaType> typeMap = this.schema.getSchemaTypes();
			this.types = new ArrayList<XmlSchemaType>(typeMap.values());
			Comparator<XmlSchemaType> compT = new CompareXmlSchemaType();
			Collections.sort(this.types, compT);
			
			//	3.  Set up the document output file.
			String outputFileName = this.properties.getProperty("doc.output");
			this.out = new DocWriter(outputFileName);
			String format = this.properties.getProperty("doc.format", "html");
			DocWriter.format = format;
									
		}
		catch (IOException io) {
			System.out.println("An IO exception was encountered: "+io.getMessage());			
		}
		catch (Exception ex) {
			System.out.println("General exception found: "+ex.getMessage());

		}
		finally {
			//	Close the input and outputs.
			try {
				if (in != null) {
					in.close();
					in = null;
				}
			}
			catch (Exception ee) {
				System.out.println("Exception on closing streams: "+ee.getMessage());
			}
		}
	}
	
	/**
	 * Close and flush.
	 */
	public void closeProject() {
		if (this.out != null) {
			this.out.close();
			this.out = null;
		}
	}
	
	/**
	 * Generate the documentation from the project specifications.
	 */
	public void generate() {
		this.generateHeader();
		this.generateElementSummary();			//	Summary of elements in a table form.
		this.generateElementDetails();
		//this.generateTypesSummary();				//	Summary of all schema types.
		//this.generateSimpleTypeDetailsSection();	//	Details of all simple schema types.
		//this.generateComplexTypeDetailsSection();	//	Details of all complex schema types.
		this.generateFooter();
	}
	
	/**
	 * Generate the document header:  title, etc.
	 */
	public void generateHeader() {
		String title = this.properties.getProperty("doc.name", "UNKNOWN SCHEMA");
		String url = this.properties.getProperty("doc.url", "UNKNOWN URL");
		String dateStr = new Date().toString();
				
		//	If we are doing HTML, output the header and body sections.
		if (DocWriter.format.compareTo(DocWriter.FORMAT_HTML) == 0) {
			this.out.printlnIndent(0, "<html>");
			this.out.printlnIndent(0, "<header>");
			this.out.printlnIndent(1, "<title>"+title+"</title>");
			this.out.printlnIndent(0, "</header>");
			this.out.printlnIndent(0, "<body>");
		}
		
		//  Title.
		this.out.header(1, 1, title);

		//	Version info.
		String version = this.schema.getVersion();
		if (version != null)
			this.out.line(1, "Schema Version: "+version);
		this.out.line(1, "Reference URL: "+url);
		this.out.line(1, "Generated on: "+dateStr);
		this.out.line(1, "Generated by XmlSchemaDoc - "+Format.link("mailto:mnorton@ms3-inc.com", "mnorton"));
		
		//	Disclaimer.
		this.out.line(1, "");
		this.out.line(1, Format.bold("Disclaimer:"));
		this.out.printlnIndent(1, "This documentation is a best level-of-effort to extract useful information from an XML Schema.");
		this.out.printlnIndent(1, "It turns out that XML Schema documents are rather complex and there is more than one way to code them, ");
		this.out.printlnIndent(1, "so it is possible that some got left out.  Send me email if you find a problem.");
		this.out.line(1, "");
		
		//	Overview
		String overview = this.properties.getProperty("overview");
		if (overview != null) {
			this.out.line(1, "");
			this.out.line(1, Format.bold("Overview:"));
			this.out.printlnIndent(1, overview);
		}
	}

	/**
	 * Generate a summary section of all element names.
	 */
	public void generateElementSummary() {
		this.out.header(1, 2, "1. Schema Element Summary");

		String rootName = this.properties.getProperty("schema.root", "Unknown");
		this.out.line(2, "Schema root element is:  "+Format.link("#"+rootName, rootName));
		this.out.line(2, "");

		this.out.printlnIndent(1, Format.table(Order.START));
		String[] headers = new String[4];
		headers[0] = "Num";
		headers[1] = "Element Name";
		headers[2] = "Type";
		headers[3] = "Description";
		this.out.printlnIndent(2, Format.header(headers));
		
		int num = 1;
		for (XmlSchemaElement element : this.elements) {
			String[] cols = new String[4];
			
			//	Get the element number.
			cols[0] = String.valueOf(num++);
			
			//	Get the element name.
			String elementName = element.getQName().getLocalPart();
			cols[1] = Format.link("#"+elementName, elementName);
			
			//	Get the type.
			cols[2] = element.getSchemaTypeName().getLocalPart();
			
			//	Extract any annotation text.
			XmlSchemaAnnotation notes = element.getAnnotation();
			cols[3] = this.extractAnnotationText(notes);

			//	Output the formatted element summary line.
			this.out.printlnIndent(2, Format.row(cols));
		}
		
		this.out.printlnIndent(1, Format.table(Order.END));
	}
	
	/**
	 * Generate a summary section of all element names.
	 */
	public void generateElementDetails() {
		this.out.header(1, 2, "2. Schema Element Details");
		
		int num=1;
		for (XmlSchemaElement element : this.elements) {
			this.generateElement(num++, element);
		}
	}
	
	/**
	 * Generate the details of a given scheme element.
	 * 
	 * @param num
	 * @param element
	 */
	public void generateElement(int num, XmlSchemaElement element) {
		String name = element.getQName().getLocalPart();
		
		this.out.anchor(name);
		this.out.line(2, "");
		this.out.header(1, 3, "2."+num+" "+name);
		
		//	Check for an element annotation.
		XmlSchemaAnnotation elementNotes = element.getAnnotation();
		if (elementNotes != null) {
			String elementNoteText = this.extractAnnotationText(elementNotes);
			this.out.line(2, elementNoteText);
		}
		
		//	Check for an element doc property.
		String elementDoc = this.properties.getProperty("element."+name);
		if (elementDoc != null) {
			this.out.printlnIndent(1, elementDoc);
			this.out.line(1, "");
		}
		
		String typeName = element.getSchemaTypeName().getLocalPart();
		
		XmlSchemaType type = element.getSchemaType();
		//XmlSchemaType type = schema.getTypeByName(typeName);		//	Doesn't make any difference.
		
		//	If the type is null, try getting it by name.
		if (type == null) {
			type = schema.getTypeByName(typeName);
		}
		
		//	Check to see if the element is a complex type.
		if (type != null && type instanceof XmlSchemaComplexType) {
			XmlSchemaComplexType complex = (XmlSchemaComplexType)type;
			this.out.line(2, "");
			this.out.line(2, "Element Type:  "+typeName+" - (Complex)");
			
			//	Get any annotations.
			XmlSchemaAnnotation notes = complex.getAnnotation();
			if (notes != null) {
				String noteText = this.extractAnnotationText(notes);
				this.out.line(2, noteText);
			}
			
			//	Check for Attributes.
			List<XmlSchemaAttributeOrGroupRef> attribs = complex.getAttributes();
			if (attribs.size() > 0) {
				this.out.line(2, "");
				this.out.line(2, Format.bold("Attributes"));
				int attNum = 1;
				for (XmlSchemaAttributeOrGroupRef attOrRef : attribs) {
					//  Check for an attribute.
					if (attOrRef instanceof XmlSchemaAttribute) {
						XmlSchemaAttribute att = (XmlSchemaAttribute)attOrRef;
						this.generateAttribute(attNum++, att);
					}
					
					//	Check for an attribute group reference.
					else if (attOrRef instanceof XmlSchemaAttributeGroupRef) {
						//XmlSchemaAttributeGroupRef groupRef = (XmlSchemaAttributeGroupRef)attOrRef;
						//XmlSchemaRef<XmlSchemaAttributeGroup> ref = groupRef.getRef();
						this.out.line(2, "Content has a group attribute reference.");
						
					}
				}				
			}
			
			//	Get Content.
			XmlSchemaContentModel contentModel = complex.getContentModel();
			if (contentModel != null) {				
				XmlSchemaContent content = contentModel.getContent();
				if (content != null) {
					
					/***  Look for Attributes.  ***/
					List<XmlSchemaAttributeOrGroupRef> attOrRefs = new ArrayList<XmlSchemaAttributeOrGroupRef>();
					
					//	Get the list of attributes or references.
					if (content instanceof XmlSchemaComplexContentExtension)
						attOrRefs = ((XmlSchemaComplexContentExtension)content).getAttributes();
					else if (content instanceof XmlSchemaComplexContentRestriction)
						attOrRefs = ((XmlSchemaComplexContentRestriction)content).getAttributes();
					else
						attOrRefs = complex.getAttributes();
					
					/*  This doesn't add much to the documentaiton.
					if (attOrRefs.size() > 0)
						this.out.line(2, "This element has "+attOrRefs.size()+" attributes:");
					else
						this.out.line(2, "Element has no attributes in the content model.");
					*/
					this.out.line(2, "");
					this.out.line(2, Format.bold("Attributes"));
					
					int attNum = 1;
					for (XmlSchemaAttributeOrGroupRef attOrRef : attOrRefs) {
						//  Check for an attribute.
						if (attOrRef instanceof XmlSchemaAttribute) {
							XmlSchemaAttribute att = (XmlSchemaAttribute)attOrRef;
							this.generateAttribute(attNum++, att);
						}
						
						//	Check for an attribute group reference.
						else if (attOrRef instanceof XmlSchemaAttributeGroupRef) {
							//XmlSchemaAttributeGroupRef groupRef = (XmlSchemaAttributeGroupRef)attOrRef;
							//XmlSchemaRef<XmlSchemaAttributeGroup> ref = groupRef.getRef();
							this.out.line(2, "Content has a group attribute reference.");
							
						}
					}
					
					/*** Look for Particles.  ***/
					if (content instanceof XmlSchemaComplexContentExtension) {
						XmlSchemaComplexContentExtension extension = (XmlSchemaComplexContentExtension)content;
						XmlSchemaParticle particle = extension.getParticle();
						
						this.out.line(2, "");
						this.out.line(2, Format.bold("Element Extension"));
						this.generateParticle(particle);
					}
					else if (content instanceof XmlSchemaComplexContentRestriction) {
						XmlSchemaComplexContentRestriction restriction = (XmlSchemaComplexContentRestriction)content;
						XmlSchemaParticle particle = restriction.getParticle();

						this.out.line(2, "");
						this.out.line(2, Format.bold("Element Restriction"));
						this.generateParticle(particle);
					}
				}
				else
					this.out.line(2, "Element has no content.");
			}
			else {
				this.out.line(2, "Element has no content model.");
				//System.out.println("\n"+typeName);
				//System.out.println(type.toString());
			}
			
		}
		
		//	Check to see if the element is a simple type.
		else if (type != null && type instanceof XmlSchemaSimpleType) {
			XmlSchemaSimpleType simple = (XmlSchemaSimpleType)type;
			this.out.line(2, "");
			this.out.line(2, "Element Type:  "+typeName+" - (Simple)");
			
			//	Get any annotations.
			XmlSchemaAnnotation notes = simple.getAnnotation();
			if (notes != null) {
				String noteText = this.extractAnnotationText(notes);
				this.out.line(2, noteText);
			}

			this.generateSimpleType(1, simple);
		}
		
		//	At the very least, check for a type annotation.
		else {
			//	Get any annotations.
			XmlSchemaAnnotation notes = type.getAnnotation();
			if (notes != null) {
				String noteText = this.extractAnnotationText(notes);
				this.out.line(2, noteText);
			}			
		}
		
	}

	/**
	 * Generate documentation for an attribute.
	 * 
	 * @param att - XmlSchemaAttribute
	 */
	public void generateAttribute(int num, XmlSchemaAttribute att) {
		//	Show the attribute name.
		String attName = att.getQName().getLocalPart();
		this.out.line(2, "");
		this.out.line(2, num+":  "+Format.bold(attName));

		//	Show any annotations.
		XmlSchemaAnnotation attNotes = att.getAnnotation();
		if (attNotes != null) {
			String attNoteText = this.extractAnnotationText(attNotes);
			this.out.line(2, attNoteText);
		}

		//	Check for an element doc property.
		String attributeDoc = this.properties.getProperty("attribute."+attName);
		if (attributeDoc != null) {
			this.out.printlnIndent(2, attributeDoc);
			this.out.line(1, "");
		}

		//	Show the default value, if any.
		String attDefault = att.getDefaultValue();
		if (attDefault != null)
			this.out.line(2, "Default value: "+attDefault);
		
		//	Show the fixed value, if any.
		String attFixed = att.getFixedValue();
		if (attFixed != null)
			this.out.line(2, "Fixed value: "+attFixed);
		
		//	Show attribute type and values, if any.
		QName schemaTypeQname = att.getSchemaTypeName();
		if (schemaTypeQname != null){
			String attTypeName = schemaTypeQname.getLocalPart();
			this.out.line(2, "Attribute type:  "+attTypeName);
			XmlSchemaType type = this.schema.getTypeByName(attTypeName);
			if (type != null && type instanceof XmlSchemaSimpleType)
				this.generateSimpleType(num, (XmlSchemaSimpleType)type);
		}
		
		/*
		XmlSchemaSimpleType simple = att.getSchemaType();
		if (simple != null)
			this.generateSimpleType(1, simple);
		else
			this.out.line(2, "Not a simple type.");
		*/
	}
	
	/**
	 * WARNING:  This method uses recursion to walk down sequence/choice trees.
	 * 
	 * HACK:  For a reason unknown to me, element names get lost after recursion.  However, the reference strings still
	 * point to the element.  As such, I trimmed the ref.toString() to get at the element name.
	 * 
	 * @param particle
	 */
	public void generateParticle(XmlSchemaParticle particle) {
		//this.out.line(2, "*** Particle description goes here.");
		
		//	Check for a sequence.
		if (particle instanceof XmlSchemaSequence) {
			this.out.line(2, "Sequence consists of:");
			XmlSchemaSequence sequence = (XmlSchemaSequence)particle;
			
			List<XmlSchemaSequenceMember> sequenceMembers = sequence.getItems();
			if (sequenceMembers.size() == 0) {
				this.out.line(2, "Any element.");
			}
			else {
				for (XmlSchemaSequenceMember sequenceMember : sequenceMembers) {
					if (sequenceMember instanceof XmlSchemaElement) {
						XmlSchemaElement sequenceElement = (XmlSchemaElement)sequenceMember;
						String sequencElementName = sequenceElement.getName();
						if (sequencElementName != null)
							this.out.line(2, Format.link("#"+sequencElementName, sequencElementName));
						else {
							XmlSchemaRef<?> ref = sequenceElement.getRef();
							String refStr = ref.toString();
							String elementName = refStr.substring(refStr.indexOf('}')+1);
							this.out.line(2, Format.link("#"+elementName, elementName));								
						}
					}
					else if (sequenceMember instanceof XmlSchemaSequence) {
						this.out.line(2, "---");									//	Recursion marker.
						this.generateParticle((XmlSchemaParticle)sequenceMember);
					}
					else if (sequenceMember instanceof XmlSchemaChoice) {
						this.out.line(2, "---");									//	Recursion marker.
						this.generateParticle((XmlSchemaParticle)sequenceMember);					
					}
				}
			}
		}
		
		//	Check for a choice.
		else if (particle instanceof XmlSchemaChoice) {
			this.out.line(2, "Choice of:");
			XmlSchemaChoice choice = (XmlSchemaChoice)particle;
			
			List<XmlSchemaChoiceMember> choiceMembers = choice.getItems();
			if (choiceMembers.size() == 0) {
				this.out.line(2, "Any element.");				
			}
			else {
				for (XmlSchemaChoiceMember choiceMember : choiceMembers) {
					if (choiceMember instanceof XmlSchemaElement) {
						XmlSchemaElement choiceElement = (XmlSchemaElement)choiceMember;
						
						String choiceElementName = choiceElement.getName();
						if (choiceElementName != null)
							this.out.line(2, Format.link("#"+choiceElementName, choiceElementName));
						else {
							XmlSchemaRef<?> ref = choiceElement.getRef();
							String refStr = ref.toString();
							choiceElementName = refStr.substring(refStr.indexOf('}')+1);
							this.out.line(2, Format.link("#"+choiceElementName, choiceElementName));	
						}
					}
					else if (choiceMember instanceof XmlSchemaSequence) {
						this.out.line(2, "---");									//	Recursion marker.
						this.generateParticle((XmlSchemaParticle)choiceMember);
					}
					else if (choiceMember instanceof XmlSchemaChoice) {
						this.out.line(2, "---");									//	Recursion marker.
						this.generateParticle((XmlSchemaParticle)choiceMember);					
					}				
				}
			}
		}
		
		//	Other possible cases.
		else if (particle instanceof XmlSchemaAllMember) {
			this.out.line(2, "*** Found an all member.");
		
		}
		else if (particle instanceof XmlSchemaChoiceMember) {
			this.out.line(2, "*** Found a choice member.");
			
		}
		else if (particle instanceof XmlSchemaSequenceMember) {
			this.out.line(2, "*** Found a sequence member.");
		}
		else {
			this.out.line(2, "*** Found an unknown particle.");
		}
			

	}
	
	/**
	 * Generate a summary of all types:  both simple and complex.
	 */
	public void generateTypesSummary() {
		this.out.header(1, 2, "2. Schema Types Summary");
		for (XmlSchemaType type : this.types) {
			//	Get the type name.
			String name = type.getName();
			
			//	Determine the type flavor.
			String typeText = "Other Type";
			if (type instanceof XmlSchemaSimpleType)
				typeText = "Simple Type";
			else if (type instanceof XmlSchemaComplexType)
				typeText = "Complex Type";

			this.out.line(1, Format.bold(name) + " - " + typeText);
		}
	}
	
	/**
	 * Generate a section on simple types.
	 */
	public void generateSimpleTypeDetailsSection() {
		this.out.header(1, 2, "3. Simple Schema Types - Details");
		
		int num = 1;
		for (XmlSchemaType type : this.types) {
			if (type instanceof XmlSchemaSimpleType) {
				String name = type.getQName().getLocalPart();
				this.out.header(1, 3, "3."+num+" "+name);
				this.generateSimpleType(num++, (XmlSchemaSimpleType)type);
			}
		}
	}
	
	/**
	 * Generate a section on complex types.
	 */
	public void generateComplexTypeDetailsSection() {
		this.out.header(1, 2, "4. Complex Schema Types - Details");
		
		int num = 1;
		for (XmlSchemaType type : this.types) {
			if (type instanceof XmlSchemaComplexType)
				this.generateComplexType(num++, (XmlSchemaComplexType) type);
		}
	}

	/**
	 * Generate a sub-section for a simple type.
	 * @param type
	 */
	public void generateSimpleType(int num, XmlSchemaSimpleType type) {

		XmlSchemaSimpleTypeContent content = type.getContent();
		
		if (content != null) {
			//	Check for an annotation and include it if present.
			XmlSchemaAnnotation notes = content.getAnnotation();
			if (notes != null) {
				String noteText = this.extractAnnotationText(notes);
				this.out.line(3, noteText);
			}

			//	See if this is a type restriction.
			if (content instanceof XmlSchemaSimpleTypeRestriction) {
				XmlSchemaSimpleTypeRestriction restriction = (XmlSchemaSimpleTypeRestriction)content;
				
				this.out.line(3, "A simple type restricted to one of the following values: ");
				
				List<XmlSchemaFacet> facets = restriction.getFacets();
				if (facets.size() > 0) {
					this.out.printlnIndent(1, Format.list(Order.START));
					for (XmlSchemaFacet facet : facets) {
						if (facet instanceof XmlSchemaEnumerationFacet) {
							XmlSchemaEnumerationFacet enumFacet = (XmlSchemaEnumerationFacet)facet;
							Object value = enumFacet.getValue();
							this.out.printlnIndent(4, Format.item(Format.List.UNORDERED, value.toString()));
						}
					}
					this.out.printlnIndent(3, Format.list(Order.END));
				}
			}
		}
		
	}
	
	/**
	 * Generate a sub-section for a complex type.
	 * @param type
	 */
	public void generateComplexType(int num, XmlSchemaComplexType type) {
		String name = type.getQName().getLocalPart();
		this.out.header(1, 3, "4."+num+" "+name);
		
	}
	
	/**
	 * Generate a footer section, if needed
	 */
	public void generateFooter() {
		//	If we are doing HTML, close the body section.
		if (DocWriter.format.compareTo(DocWriter.FORMAT_HTML) == 0) {
			this.out.printlnIndent(0,"</body>");
			this.out.printlnIndent(0,"</html>");
		}		
	}
	
	/**
	 * Extracts all text from annotation items. If the annotation is null, an empty string is returned.
	 * 
	 * @param note
	 * @return
	 */
	public String extractAnnotationText(XmlSchemaAnnotation note) {
		StringBuffer noteText = new StringBuffer();
		if (note != null) {
			List<XmlSchemaAnnotationItem> items = note.getItems();
			for (XmlSchemaAnnotationItem item : items) {
				if (item instanceof XmlSchemaDocumentation) {
					XmlSchemaDocumentation doc = (XmlSchemaDocumentation) item;
					NodeList nodes = doc.getMarkup();
					for (int i=0; i<nodes.getLength(); i++) {
						Node node = nodes.item(i);
						noteText.append(node.getTextContent());
					}
				}
			}				
		}
		return noteText.toString().trim();
	}

}
